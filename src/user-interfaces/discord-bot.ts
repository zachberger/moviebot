import * as Discord from 'discord.js';
import { execute, IResponder } from '../core/commands';
import { ILocator } from '../core/sessionState';
import { handleMessage } from '../utils';
import { IRepository } from './../database/index';

export default class MovieBot {
  private client = new Discord.Client();

  constructor(private discordToken: string, private repository: IRepository) {
    this._registerEvents();
    this._login(this.discordToken);
  }

  private _registerEvents() {
    this.client.on('ready', () => {
      console.log(`logged in as ${this.client.user.tag}!`);
    });

    this.client.on('message', (message) => {
      const parsed = handleMessage(message.content);
      if (parsed && parsed.length > 0) {
        const responder: IResponder = message;
        const identifier: ILocator = {
          locatorId: message.guild.id,
        };
        execute({
          locator: identifier,
          repo: this.repository,
          responder,
          userCommand: parsed,
          user: { 
            id: message.member.id
          }
        });
      }
    });
  }

  private _login(token: string) {
    this.client.login(token);
  }
}
