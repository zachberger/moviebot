import { execute, IResponder } from "../core/commands";
import { IRepository } from '../database';
import { messages } from '../utils/constants';
import { ILocator, IUser } from './../core/sessionState';

const output: IResponder = {
  reply: (message) => { console.log(message); },
};

export default function Cli(command: string[], repository: IRepository, locator: ILocator, user: IUser) {
  if (command && command.length > 0) {
    execute({
      locator: locator,
      repo: repository,
      responder: output,
      userCommand: command,
      user: user
    }).catch((err) => {
      output.reply(err);
    });
  } else {
    output.reply(messages.commandNotFound(command));
  }
}
