import { IServer, VotingPhase, IUser, IIdenifiable } from './sessionState';
import { messages } from '../utils/constants';

export class Validator {
  private validators: IValidationTask[];

  constructor(private state: IServer) {
    this.validators = [];
  }

  public addValidator(task: IValidationTask): Validator {
    this.validators.push(task);
    return this;
  }

  public runValidation(): IValidationResult {
    let result = { success: false };
    this.validators.forEach(v => {
      result = v.validate(this.state);
      if(!result.success) {
        return;
      }
    });
    return result;
  }
}

// export type IValidator = (response: IServer) => IValidationResult;


export interface IValidationTask {
  validate: (state: IServer) => IValidationResult
}

export interface IValidationResult {
  success: boolean
  message?: string
}

// :  { [validatorType: string]: IValidationTask || () => IValidationTask; }
export const VotingValidators = {
  isVoting: { validate: (state: IServer) => isVoting(state) },
  isNotVoting: { validate: (state: IServer) => isNotVoting(state) },
  hasNoSeason: { validate: (state: IServer) => hasNoSeason(state) },
  hasMinimumVoters: { validate: (state: IServer) => hasMinimumVoters(state) },
  isInPhase: (phase: VotingPhase) => { return { validate: (state: IServer) => isInPhase(state, phase) }},
  userNotRegistered: (user: IUser) => { return { validate: (state: IServer) => userNotRegistered(state, user)}},
  notNominated: (item: IIdenifiable) => { return { validate: (state: IServer) => notNominated(state, item)}},
}

function hasNoSeason(state: IServer): IValidationResult {
  if (typeof state.currentSeason !== 'undefined') {
    return {
      success: false,
      message: messages.seasonInProgress,
    }
  }
  return {
    success: true,
  }
}

function isNotVoting(state: IServer): IValidationResult {
  if (typeof state.voting !== 'undefined') {
    return {
      success: false,
      message: messages.isElection,
    }
  }
  return {
    success: true,
  }
}

function isVoting(state: IServer): IValidationResult {
  if (typeof state.voting === 'undefined') {
    return {
      success: false,
      message: messages.noElection,
    }
  }
  return {
    success: true,
  }
}

function isInPhase(state: IServer, phase: VotingPhase): IValidationResult {
  if (state.voting.phase !== phase) {
    return {
      success: false,
      message: messages.notInPhase(phase),
    }
  }
  return {
    success: true,
  }
}

function userNotRegistered(state: IServer, user: IUser): IValidationResult {
  if (state.voting.voters.find(
    voter => {
    return voter.user.id === user.id;
  })) {
    return {
      success: false,
      message: messages.alreadyRegistered,
    }
  }
  return {
    success: true,
  }
}

function notNominated(state: IServer, item: IIdenifiable): IValidationResult {
  if (state.voting.nominations.find(
    nomination => {
      return nomination.item.id === item.id;
    }
  )) {
    return {
      success: false,
      message: messages.alreadyNominated(item.id),
    }
  }
  return {
    success: true,
  }
}

function hasMinimumVoters(state: IServer): IValidationResult {
  const voterLimit = 2;

  if (state.voting.voters.length < voterLimit) {
    return {
      success: false,
      message: messages.notEnoughVoters(state.voting.voters.length, voterLimit),
    }
  }
  return {
    success: true,
  }
}