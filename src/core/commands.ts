import { Validator, VotingValidators } from './validators';
import { messages } from './../utils/constants';
import { IRepository } from './../database';
import { ILocator, VotingPhase, IUser } from './sessionState';

export interface ICommand {
  userCommand: string[];
  responder: IResponder;
  repo: IRepository;
  locator: ILocator;
  user: IUser;
}

export interface IResponder {
  reply: (message) => void;
}

type CommandLookup = { [commandKey: string]: (command: ICommand) => Promise<void>; };
const commands: CommandLookup = {
  endRegistration: (com: ICommand) => endRegistration(com),
  info: (com: ICommand) => info(com),
  nominate: (com: ICommand) => nominate(com),
  ping: (com: ICommand) => ping(com),
  register: (com: ICommand) => register(com),
  startSeason: (com: ICommand) => startSeason(com),
  // notFound: new NotFoundCommand(),
};

/**
 * Right now some of your commands return a promise, and others return a value.
 * Typically commands all have the same signature, to begin enforcing this,
 * create an interface with one method, execute, and have each command implement
 * this interface I would consider changing ICommand to IMessage, and having 
 * the command interface be ICommand. 
 * 
 * I'd also consider a file per command.
 * 
 * Also in regards to typings, when you mark a function as async the typesystem 
 * can infer it returns a promise. That being said, there is nothing wrong with 
 * explicitly typing your functions.
 */

export async function execute(com: ICommand): Promise<void> {
  const input = com.userCommand;
  if (input && input.length > 0) {
    const base = input[0];
    if (commands.hasOwnProperty(base)) {
      return commands[base](com);
    }
  }
  return notFound(com);
}

// NotFound.execute(message)
function notFound(com: ICommand): void {
  const reply = messages.commandNotFound(com.userCommand[0]);
  com.responder.reply(reply);
}

function ping(com: ICommand): void {
  const reply = 'Pong!';
  com.responder.reply(reply);
}

async function info(com: ICommand): Promise<void> {
  const reply = await com.repo.getServerState(com.locator);
  com.responder.reply(JSON.stringify(reply));
}

async function startSeason(com: ICommand): Promise<void> {
  const state = await com.repo.getServerState(com.locator);
  const result = new Validator(state)
    .addValidator(VotingValidators.hasNoSeason)
    .addValidator(VotingValidators.isNotVoting)
    .runValidation();

  if (result.success) {
    state.voting = {
      phase: VotingPhase.registration,
      nominations: [],
      voters: [],
      round: 0,
    }
  
    await com.repo.setServerState(state);
  
    com.responder.reply(messages.newSeasonStarted);
  } else { 
    com.responder.reply(result.message);
  }
}

async function register(com: ICommand): Promise<void> {
  const state = await com.repo.getServerState(com.locator);
  const result = new Validator(state)
    .addValidator(VotingValidators.isVoting)
    .addValidator(VotingValidators.isInPhase(VotingPhase.registration))
    .runValidation();

  if (result.success) {
    state.voting.voters.push({ 
      user: com.user, 
      points: 0, 
      votes: [] 
    });

    await com.repo.setServerState(state);

    com.responder.reply(messages.registeredToVote);
  
  } else {
    com.responder.reply(result.message);
  }
}

async function endRegistration(com: ICommand): Promise<void> {
  const state = await com.repo.getServerState(com.locator);
  const result = new Validator(state)
    .addValidator(VotingValidators.isVoting)
    .addValidator(VotingValidators.isInPhase(VotingPhase.registration))
    .addValidator(VotingValidators.hasMinimumVoters)
    .runValidation();

  if (result.success) {
    state.voting.phase = VotingPhase.nomination;
    
    await com.repo.setServerState(state);

    com.responder.reply(messages.registrationPhaseComplete);
  } else {
    com.responder.reply(result.message)
  }
}

async function nominate(com: ICommand): Promise<void> {
  const state = await com.repo.getServerState(com.locator);
  const nomination = { id: com.userCommand[1] };
  const result = new Validator(state)
    .addValidator(VotingValidators.isVoting)
    .addValidator(VotingValidators.isInPhase(VotingPhase.nomination))
    .addValidator(VotingValidators.notNominated(nomination))
    .runValidation();

  if (result.success) {
    state.voting.nominations.push({ 
      votes: 0, 
      item: { 
        id: com.userCommand[1] 
      }
    });

    await com.repo.setServerState(state);

    com.responder.reply(messages.nominationAccepted);
  } else {
    com.responder.reply(result.message);
  }
}

