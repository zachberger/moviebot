import { ILocator } from './sessionState';
export interface ILocator {
  locatorId: string;
}

export interface IServer extends ILocator {
  currentSeason?: ISeason;
  currentMovieNight?: IMovieNight;
  voting?: IVoting;
}

export interface IArchive {
  locator: ILocator;
  movieNights?: IMovieNight[];
  movies?: IMovie[];
  genres?: IGenre[];
  seasons?: ISeason[];
}

export interface ISeason {
  genres: IGenre[];
  start: Date;
}

export interface IIdenifiable {
  id: string
}

export interface IGenre extends IIdenifiable {
  name: string;
}

export interface IMovieNight {
  night: Date;
  movie: IMovie;
  genre: IGenre;
}

export interface IMovie extends IIdenifiable {
  name: string;
  genres: IGenre[];
}

export interface IUser {
  id: string;
}

export interface IVoting {
  phase: VotingPhase;
  nominations: IVotable[];
  voters: IVoter[];
  round: number;
  chosen?: IIdenifiable;
}

export interface IVoter {
  user: IUser;
  points: number;
  votes: IVotable[];
}

export interface IRound {
  items: IVotable[];
  number: number;
  chosen: IVotable;
}

export interface IVotable {
  votes: number;
  item: IIdenifiable;
}

export enum VotingPhase {
  registration,
  nomination,
  vote,
  veto,
  complete,
}
