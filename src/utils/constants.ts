export const commandId = '!';

export const messages = {
  alreadyRegistered: 'You are already registered to vote in this election!',
  alreadyNominated: (item) => `${item} already nominated`,
  commandNotFound: (command) => `Command could not be found ${command}`,
  noElection: 'There is no election in progress.',
  isElection: 'There is an election currently in progress.',
  noRegistration: 'Cannot register to vote.',
  noNomination: 'Cannot nominate for vote.',
  notInPhase: (phase) => `Voting is not in the ${phase} phase.`,
  seasonInProgress: 'Season currently in progress. End this one before beginning a new one',
  notEnoughVoters: (count, limit) => `Not enough voters. Currently there are ${count} and ${limit} are needed.`,
  newSeasonStarted: 'New season started. Please register to vote.',
  registeredToVote: 'You are registered to vote!',
  registrationPhaseComplete: 'Registration phase not commplete. Moving on to the nominations.',
  invalidArgument: (command) => `You did not enter a valid argument with the command ${command}`,
  nominationAccepted: 'Your nomination has been accepted!',
}
