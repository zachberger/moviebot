import { commandId } from './constants';

export function handleMessage(message: string): string[] {
  const messageHasCommandIdAtBeginning = message.indexOf(commandId) === 0;
  
  if (messageHasCommandIdAtBeginning) {
    const breakdown = message.substr(commandId.length).split(' ') || [];
    return breakdown;
  }
  return [];
}
