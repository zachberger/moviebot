import { ILocator, IServer } from './../core/sessionState';
import { IRepository } from './index';

export interface IRepository {
  getServerState: (locator: ILocator) => Promise<IServer>;
  setServerState: (serverState: IServer) => Promise<IServer>;
}

export { mongoRepo } from './mongo';
