import { MongoClient, Collection } from 'mongodb';
import config from '../config';
import { ILocator, IServer } from '../core/sessionState';
import { IRepository } from './index';

export const mongoRepo: IRepository = {
  getServerState: (locator: ILocator) => getServerState(locator),
  setServerState: (serverState: IServer) => setServerState(serverState),
};

async function getServerState(locator: ILocator): Promise<IServer> {
  return await execAgainstStateCollection(async (col: Collection) => {
    let result = await col.findOne(locator);
    if (!result) {
      const insertResult = await col.insertOne({
        created: Date.now(),
        // I had no idea the spread operator worked on objects
        ...locator
      });
      // you were doing this twice in the case you weren't inserting
      // Consider filtering on insertResult.insertedId instead of
      // locator. id is guaranteed to be indexed so the findOne 
      // should be fastr
      result = await col.findOne({_id: insertResult.insertedId});
    }

    return result;
  });
}

// What does the return type look like?
async function execAgainstStateCollection(execFunc) {
  // Opening and closing connections is super expensive, consider making this a member 
  // of your class, and reusing it. 
  const client = await MongoClient.connect(config.db.url, { useNewUrlParser: true });
  const db = client.db(config.db.name);
  const col = db.collection(config.db.sessionStateCollection);

  const result = await execFunc(col);

  client.close();

  return result;
}

async function setServerState(state: IServer): Promise<IServer> {
  return await execAgainstStateCollection(async (col: Collection) => {
    const result = await col.updateOne({ locatorId: state.locatorId }, { $set: state });
    return result;
  });
}

// class Monogo {
//   private _client;
//   constructor() {
//     this._client = MongoClient.connect(config.db.url, { useNewUrlParser: true });
//   }

//   async function exec() {
//     await this._client;
//     this._client.

//   }

// }