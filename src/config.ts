const config = {
  db: {
    name: 'moviebot',
    sessionStateCollection: 'sessionStateCollection',
    url: 'mongodb://localhost:27017',
  },
};

export default config;
