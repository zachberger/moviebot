import * as Commander from 'commander';
import { mongoRepo } from './database/mongo';
import { secrets } from './secrets';
import MovieBotCli from './user-interfaces/command-line';
import MovieBot from './user-interfaces/discord-bot';

Commander
  .version('1.0.0')
  .option('--cli', 'Command line interface.')
  .option('--locator-id <a>', 'Guild and Channel Ids.', '1')
  .option('--user-id <a>', 'User Id.', '1')
  .parse(process.argv);

const repo = mongoRepo;

if (Commander.cli) {
  const args = process.argv.filter((e) => {
    return e !== '--cli';
  }).splice(2);
  MovieBotCli(
    args,
    repo,
    {
      locatorId: Commander.locatorId,
    },
    {
      id: Commander.userId,
    },
  );
} else {
  const DISCORD_TOKEN = secrets.discordBotToken;
  new MovieBot(DISCORD_TOKEN, repo);
}
