# Consider reading https://cloud.google.com/solutions/best-practices-for-building-containers
FROM node:11.1.0-alpine
WORKDIR /app
COPY package.json /app
## Consider building the app outside the container then just moving the app in
RUN npm install
COPY ./dist /app
# Not sure this is the best way to start the process, also what if it crashes?
# Consider using https://github.com/just-containers/s6-overlay  
CMD node index.js